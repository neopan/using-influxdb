package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api/write"

	"github.com/joho/godotenv"
)

const (
	org    = "m3c"
	bucket = "one-day"
	points = 5
)

func main() {
	if err := godotenv.Load(); err != nil {
		log.Fatal("Error loading .env file")
	}

	token := os.Getenv("INFLUXDB_TOKEN")
	url := "http://localhost:8086"
	client := influxdb2.NewClient(url, token)
	defer client.Close()

	fmt.Println("------------------------------------------------")
	fmt.Println("org:", org)
	fmt.Println("bucket:", bucket)
	fmt.Println("------------------------------------------------")

	ctx := context.Background()
	ts := time.Now()

	writeAPI := client.WriteAPIBlocking(org, bucket)
	for value := 0; value < points; value++ {
		tags := map[string]string{"tagname1": "tagvalue1"}
		fields := map[string]any{"field1": value}
		point := write.NewPoint("measurement1", tags, fields, ts)
		// time.Sleep(1 * time.Second) // separate points by 1 second
		ts = ts.Add(1 * time.Second)

		if err := writeAPI.WritePoint(ctx, point); err != nil {
			log.Fatal(err)
		}
	}

	fmt.Println("------------------------------------------------")
	queryAPI := client.QueryAPI(org)
	query := `from(bucket: "one-day")
            |> range(start: -10m)
            |> filter(fn: (r) => r._measurement == "measurement1")`
	fmt.Println(query)
	fmt.Println("------------------------------------------------")

	results, err := queryAPI.Query(context.Background(), query)
	if err != nil {
		log.Fatal(err)
	}
	for results.Next() {
		fmt.Println(results.Record())
	}
	if err := results.Err(); err != nil {
		log.Fatal(err)
	}

	fmt.Println("------------------------------------------------")
	query = `from(bucket: "one-day")
              |> range(start: -10m)
              |> filter(fn: (r) => r._measurement == "measurement1")
              |> mean()`
	fmt.Println(query)
	fmt.Println("------------------------------------------------")

	results, err = queryAPI.Query(ctx, query)
	if err != nil {
		log.Fatal(err)
	}
	for results.Next() {
		fmt.Println(results.Record())
	}
	if err := results.Err(); err != nil {
		log.Fatal(err)
	}
}
